#ifndef BUTTONS_DEFS
#define BUTTONS_DEFS

#include "iom16.h"

#define BUTTON_BREAK_MASK     0x02
#define BUTTON_CENTER_MASK    0x10
#define BUTTON_LEFT_MASK      0x04
#define BUTTON_RIGHT_MASK     0x40
#define BUTTON_UP_MASK        0x08
#define BUTTON_DOWN_MASK      0x20

#define IS_LEFT_PRESSED()     (!(PINA & BUTTON_LEFT_MASK))  //(1<<PA2)))
#define IS_RIGHT_PRESSED()    (!(PINA & BUTTON_RIGHT_MASK))  //(1<<PA6)))
#define IS_UP_PRESSED()       (!(PINA & BUTTON_UP_MASK))  //(1<<PA3)))
#define IS_DOWN_PRESSED()     (!(PINA & BUTTON_DOWN_MASK))  //(1<<PA5)))
#define IS_CENTER_PRESSED()   (!(PINA & BUTTON_CENTER_MASK))  //(1<<PA4)))
#define IS_BREAK_PRESSED()    (!(PINA & BUTTON_BREAK_MASK))  //(1<<PA1)))
#define IS_ANY_KEY_PRESSED()  ((~PINA)& 0x7F)

extern unsigned char BUTTON_fl;

void keyboard_init();
void keyboard_handler();

//buttons pullup
#define keyboard_init()       { PORTA = 0x7E; }
#define keyboard_handler()    { BUTTON_fl = IS_ANY_KEY_PRESSED(); }
#define keyboard_block()      { while(IS_ANY_KEY_PRESSED()){}; }

#endif