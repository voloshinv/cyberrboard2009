#ifndef TERMINAL_
#define TERMINAL_

void terminal_handler();
void terminal_process_cmd();
void terminal_do_command(unsigned int cmd);

#endif