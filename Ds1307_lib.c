#include "Ds1307_lib.h"
#include "twi.h"

__flash const char* months[12] = {"������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������"};

U8 ds1307_check_flag = 0;

// Input range - 00 to 99.
U8 bcd2bin(U8 bcd_value)
{
  U8 temp;

  temp = bcd_value;
  // Shifting upper digit right by 1 is same as multiplying by 8.
  temp >>= 1;
  // Isolate the bits for the upper digit.
  temp &= 0x78;

  // Now return: (Tens * 8) + (Tens * 2) + Ones

  return(temp + (temp >> 2) + (bcd_value & 0x0f));
}

U8 bin2bcd(U8 binary_value)
{
  U8 temp;
  U8 retval;

  temp = binary_value;
  retval = 0;

  while(1)
  {
    // Get the tens digit by doing multiple subtraction
    // of 10 from the binary value.
    if(temp >= 10)
    {
      temp -= 10;
      retval += 0x10;
    }
    else // Get the ones digit by adding the remainder.
    {
      retval += temp;
      break;
    }
  }

  return(retval);
}

U8 ds1307_init()
{
  U8 result;
  result =  i2c_start(0xD0);
  if (result) return 1;
  
  ds1307_check_flag = 1;
  return 0;
}

void ds1307_set_date_time (U8 day, U8 mth, U8 year, U8 dow, U8 hr, U8 min, U8 sec)
{
  if (! ds1307_check_flag) return;
  
  sec &= 0x7F;
  hr &= 0x3F;

  i2c_start(DS1307_ADDRESS | TWI_WRITE);
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_write(bin2bcd(sec));      // REG 0
  i2c_write(bin2bcd(min));      // REG 1
  i2c_write(bin2bcd(hr));      // REG 2
  i2c_write(bin2bcd(dow));      // REG 3
  i2c_write(bin2bcd(day));      // REG 4
  i2c_write(bin2bcd(mth));      // REG 5
  i2c_write(bin2bcd(year));      // REG 6
  i2c_write(0x80);            // REG 7 - Disable squarewave output pin
  i2c_stop();
}

void ds1307_get_date(U8 *day, U8 *mth, U8 *year, U8 *dow)
{
  if (! ds1307_check_flag) return;
  
  i2c_start(DS1307_ADDRESS | TWI_WRITE);
  i2c_write(0x03);            // Start at REG 3 - Day of week
  i2c_start(DS1307_ADDRESS | TWI_READ);
  *dow  = bcd2bin(i2c_readAck() & 0x7f);   // REG 3
  *day  = bcd2bin(i2c_readAck() & 0x3f);   // REG 4
  *mth  = bcd2bin(i2c_readAck() & 0x1f);   // REG 5
  *year = bcd2bin(i2c_readNak());            // REG 6
  i2c_stop();
}

void ds1307_get_time(U8 *hr, U8 *min, U8 *sec)
{
  if (! ds1307_check_flag) return;
  
  i2c_start(DS1307_ADDRESS | TWI_WRITE);
  i2c_write(0x00);            // Start at REG 0 - Seconds
  i2c_start(DS1307_ADDRESS | TWI_READ);
  *sec = bcd2bin(i2c_readAck() & 0x7f);
  *min = bcd2bin(i2c_readAck() & 0x7f);
  *hr  = bcd2bin(i2c_readNak() & 0x3f);
  i2c_stop();
}
