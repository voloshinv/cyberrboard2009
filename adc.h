#ifndef ADC_MODULE_
#define ADC_MODULE_

void adc_init();
unsigned int read_adc(unsigned char adc_input);

#endif //ADC_MODULE_