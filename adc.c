#include  <inavr.h>
#include "adc.h"
#include "iom16.h"
#include "global.h"


#define ADC_VREF_TYPE 0x60
void adc_init()
{
  // ADC initialization
  // ADC Clock frequency: 1000,000 kHz
  // ADC Voltage Reference: AREF pin
  // ADC Auto Trigger Source: None
  // Only the 8 most significant bits of
  // the AD conversion result are used
  ADMUX=ADC_VREF_TYPE & 0xff;
  ADCSRA=0x84;
}

// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input)
{
  ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
  // Delay needed for the stabilization of the ADC input voltage
  DELAY_US(10);
  // Start the AD conversion
  ADCSRA|=0x40;
  // Wait for the AD conversion to complete
  while ((ADCSRA & 0x10)==0);
  ADCSRA|=0x10;
  return ADCH;
}
