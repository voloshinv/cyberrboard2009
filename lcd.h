/* ***********************************************************************
**
**  Copyright (C) 2005 Christian Kranz
**
** Siemens S65 Display Control
*********************************************************************** */

#ifndef _LCD_H
#define _LCD_H

#include "global.h"

#define LCD_CS	   PD6
#define LCD_RESET  PD5
#define LCD_RS	   PD4
#define LCD_MOSI   PB5
#define LCD_MISO   PB6
#define LCD_SCK    PB7

// ������� ����� ��� LCD
#define BLACK   0x0000
#define WHITE   0xFFFF
#define RED     0xF800
#define GREEN   0x07E0
#define BLUE    0x001F
#define YELLOW  0xFFE0
#define ICEBLUE (0x14<<11|0x31<<5|0x1C)
#define GRAY    (0x18<<11|0x30<<5|0x18)
#define BLACK_60  (0x10<<11|0x20<<5|0x10)

#define DISP_W 132
#define DISP_H 176
#define CHAR_H 14
#define CHAR_W 8
#define TEXT_COL 16
#define TEXT_ROW 12

void lcd_ls020_init();
void open_window(char x, char y, char x_len, char y_len);
void lcd_fillscreen(unsigned int color);
void put_string_6x8(char x, char y, char* chArr, int ColPix, int ColBgr);
void put_string_8x8(char x, char y, char* chArr, int ColPix, int ColBgr);
void set_brightness(unsigned char percent);
void set_brightness_up();
void set_brightness_down();
void show_brightness_to_user(char percent);

#endif // ifndef LCD_H

