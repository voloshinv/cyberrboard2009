#include "stdio.h"
#include "lcd.h"
#include  <inavr.h>
//#include "CharMap.h"
#include "At24C512_lib.h"
#include "twi.h"
#include "usart.h"
#include "Ds1307_lib.h"
#include "onewire.h"
#include "buttons.h"
#include "timer.h"
#include "terminal.h"
#include "adc.h"
#include "servo.h"

#define TEMPERATURE_TIMER_VALUE 800

char string_buff[50];

#define STABILIZED_VREF   4.3 // �������� ������� �� �������������
float battery_value;

U8 day, month, year, dow, hour, min, sec;
int temperature;
U8 servo1 = 50;

int rx_char_index = 0;

void Date_Time_handler();
void ow_handler();
int get_temperature(int *temp0);
void buttons_handler();
void servo_handler();

_t_dynamicTimer gTemperature_timer, gServodelay_timer;

inline void power_down_mode_init()
{
  MCUCR = 1<<SM1; //POWER DOWN
}

void goto_sleep()
{  
  //Disabling all GPIOS to avod display parasite powering
  PORTD &= ~((1<<LCD_RESET) | (1<<LCD_CS) | (1<<LCD_RS));
  SPDR = 0;
  
  //PWM Timer Disable
  TCCR2 = 0x00;

  //3.3V power disable!
  DDRC |= 1<<PC6;
  PORTC |= 1<<PC6;
  
  MCUCR |= 1<<SE;
  __sleep();
}

void buttons_handler()
{
  keyboard_handler();
  if (! BUTTON_fl) return;
  
  if (BUTTON_fl & BUTTON_UP_MASK)
  {
    if (servo1 <= 90) 
    {
      servo1 +=10;    
      servo_set(servo1);
    }
    keyboard_block();
  }
  
  if (BUTTON_fl & BUTTON_DOWN_MASK)
  {
    if (servo1 >= 10) 
    {
      servo1 -=10;    
      servo_set(servo1);
    }
    keyboard_block();
  }
  
  if (BUTTON_fl & BUTTON_LEFT_MASK)
  {
    set_brightness_down();
    keyboard_block();
  }
  
  if (BUTTON_fl & BUTTON_RIGHT_MASK)
  {
    set_brightness_up();
    keyboard_block();
  }
  
  if (BUTTON_fl & BUTTON_BREAK_MASK)
  {
    lcd_fillscreen(0x0000);
    rx_char_index = 0;
    usart_putstring("AT");
    keyboard_block();
  }
  
  if (BUTTON_fl & BUTTON_CENTER_MASK)
  {
    lcd_fillscreen(0x00F3);
    //temporary code
    ds1307_set_date_time(2, 4, 15, 4, 0, 0, 1);
    keyboard_block();
  }
  
  //���������� ��� ����� (��� ������ ���� ���������� ������)
  BUTTON_fl = 0;
}

int main( void )
{
  DDRA = 0x00;
  DDRB = 0x00;
  DDRC = 0x00;
  DDRD = 0x00;
  
  global_timer_init();
  keyboard_init();
  
  usart_init();
  usart_putstring("AT");
  
  i2c_init();
  at24c_init();
  ds1307_init();
  //power_down_mode_init();
  adc_init();
  servo_init();
  //servo_set(servo1);
  
  //ds1307_set_date_time(21, 7, 11, 4, 10, 55 , 0);
  
  lcd_ls020_init();
  lcd_fillscreen(0x00F3);
 
  put_string_6x8(2, 5, "����� 5x8 � EEPROM", RED, 0x00F3);
  put_string_8x8(2, 15, "����� 8x8 �", GREEN, 0x00F3);
  put_string_8x8(2, 25, "EEPROM", GREEN, 0x00F3);

  //InitTimer( &gTemperature_timer, TEMPERATURE_TIMER_VALUE);
  //InitTimer( &gServodelay_timer, 30);
  
  while(1){
    //at24c_wr_handler();
     buttons_handler();
     terminal_handler();
     ow_handler();
     //Date_Time_handler();
     servo_handler();
     tea_handler();
      
      sprintf(string_buff, "T = %0.3f", temperature/16.0);
      put_string_8x8(20, 90, string_buff, YELLOW, 0x00F3);
      
      battery_value = STABILIZED_VREF / 256;
      battery_value *= read_adc(0);
      
      // ���������� ������� �������� � 2 ����
      battery_value *= 2;
      
      if (battery_value > 3.2)
        Date_Time_handler();    
      else
        put_string_8x8(11, 140, "stopped", RED, 0x00F3);
      
//      battery_value +=0.5; //������������� ���������� ????
      
      sprintf(string_buff, "UBAT = %0.1f ", battery_value);
      put_string_8x8(11, 110, string_buff, GREEN, 0x00F3);
    
    /*
      U8 data;
      if (! usart_getchar((char*)&data))
      {
        sprintf(string_buff, "%c", data);
        put_string_6x8(1 + (rx_char_index%21)*6, (rx_char_index/21)*8 , string_buff, RED, 0x00F3);
        rx_char_index++;
        if (rx_char_index > 441)
        {
          lcd_fillscreen(0x0000);
          rx_char_index = 0;
        }
      }*/
    
    
    //goto_sleep();
  }
  return 0;
}

void Date_Time_handler()
{
  static char old_sec=0;
      
  ds1307_get_time( &hour, &min, &sec);
  
  if (sec != old_sec)
  {
    //refresh date    
#if 0
    ds1307_get_date( &day, &month, &year, &dow);
    month--;
    sprintf(string_buff, "%d %s 20%d", day, (month<12) ? months[month] : "???", year);
    month++;
    put_string_8x8(10, 50, string_buff, YELLOW, 0x00F3);
#endif
    
    //refresh time
    old_sec = sec;
    sprintf(string_buff, "%d : %d : %d ", hour, min, sec);
    put_string_8x8(20, 70, string_buff, RED, 0x00F3);
  }
}

void ow_handler()
{
  /*
  static uint8_t id[OW_ROMCODE_SIZE];
  static uint8_t diff = OW_LAST_DEVICE, nDevices;
  
  if (( diff == OW_PRESENCE_ERR) || ( diff == OW_DATA_ERR) || ( diff == OW_LAST_DEVICE))
    {
            usart_putstring("new search..\n\r");
            nDevices = 0;
            diff = OW_SEARCH_FIRST;
    }

    diff = ow_rom_search( diff, &id[0] );

    if ( diff == OW_PRESENCE_ERR) usart_putstring("ow presence error.\n\r");
    else if ( diff == OW_DATA_ERR) usart_putstring("ow data error.\n\r");
    else if ( diff == OW_LAST_DEVICE) usart_putstring("nothing else found.\n\r");
    else //all is ok
    {
            nDevices++;
            usart_putstring("found device:");
            for (int i=0; i<8; i++)
            {
                    char buff[10];
                    sprintf(buff, " %#X", id[i]);
                    usart_putstring(buff);
            }
            usart_putstring(" \n\r");
    }
  */
  
  int temp0;
  if (get_temperature(&temp0))
  { 
    temperature = temp0;
  }
}

int get_temperature(int *temp0)
{
#define DS18S20_CONVERT_T_CMD         0x44
#define DS18S20_READ_SCRATCHPAD_CMD   0xBE
  
  //U8 temperature[2];
  if (isTimerExpired( &gTemperature_timer))
  {
    //reading temp
    ow_parasite_disable();
    ow_reset();
    ow_byte_wr(OW_SKIP_ROM);
    ow_byte_wr(DS18S20_READ_SCRATCHPAD_CMD);
    //temperature[0] = ow_byte_rd();
    //temperature[1] = ow_byte_rd();
    
    //*temp0 = (int)temperature[1]<<8;
    //*temp0 |= temperature[0];
    
    *temp0 = ow_byte_rd() | (int)ow_byte_rd()<<8;
    
    //converting temperature
    ow_reset();
    ow_byte_wr(OW_SKIP_ROM);
    ow_byte_wr(DS18S20_CONVERT_T_CMD);
    ow_parasite_enable();
    
    InitTimer( &gTemperature_timer, TEMPERATURE_TIMER_VALUE);
    return 1;
  }
  return 0;
}

void servo_handler()
{
  if (isTimerExpired( &gServodelay_timer))
  {
    InitTimer( &gServodelay_timer, 20);
    servo_go();
  }
}
