#ifndef DS1307_LIBRARY
#define DS1307_LIBRARY

#include "global.h"

#define DS1307_ADDRESS  0xD0

U8 ds1307_init();
void ds1307_set_date_time (U8 day, U8 mth, U8 year, U8 dow, U8 hr, U8 min, U8 sec);
void ds1307_get_date(U8 *day, U8 *mth, U8 *year, U8 *dow);
void ds1307_get_time(U8 *hr, U8 *min, U8 *sec);

extern __flash const char* months[12];

#endif
