#ifndef GLOBAL_DEFINITIONS
#define GLOBAL_DEFINITIONS

#define F_CPU 16000000L
#define	CYCLES_PER_US (F_CPU / 1000000L)
#define DELAY_US(us)   __delay_cycles(CYCLES_PER_US*(us));
#define DELAY_MS(ms)   __delay_cycles(CYCLES_PER_US*1000L*(ms));

typedef unsigned long U32;
typedef unsigned int U16;
typedef unsigned char U8;

#endif
