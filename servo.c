#include "servo.h"
#include  <inavr.h>
#include "iom16.h"
#include "global.h"
#include "timer.h"

//servo_val = 0xCD = (255 - 50 �����) = 0.8 �� - ����������� ��������� ����� (�� ������������� easyelectronics)
unsigned char servo_val = 0xFF;

void servo_init()
{
  PORTB &= 0xF7;
  DDRB |= 0x08;
  
  // �������� ���������� ��� timer0 overflow
  TIMSK |= 0x01;
}

/*
  ������������� ������� ��������� �����-������
  �������� ������ ���� � ���������.
  ���� ������ �������� ��������� 255 - ����� ����������.
*/
void servo_set(unsigned char percent)
{
  if (percent == 0xFF) 
  {
    servo_val = 0xFF;
    return;
  }
  if (percent > 100) return;
  
  servo_val = (unsigned char)(0xD7 - percent * 0.9); //0.875
}

void servo_go()
{
  if (servo_val == 0xFF) return;
  
  TCNT0 = servo_val;
  TCCR0 = 0x04; //start timer with clock 62500
  //�� 1�� ������ ��������� 62.5 ����
  
  PORTB |= 0x08;
}

#pragma vector=TIMER0_OVF_vect
__interrupt void servo_stop()
{
  PORTB &= 0xF7;
  
  TCCR0 = 0x00; //stop timer
}

#define TEA_PACK_IMMERSED_SERVO_VAL   0
#define TEA_PACK_LIFTED_SERVO_VAL     50
#define SMOOTHNESS_TIME_SEC           2.0 //must be float
#define TEA_LIFTED_DELAY              3000
#define TEA_IMMERSED_DELAY            8000

unsigned char tea_toggles;
unsigned char smoothnessServoStep;
void make_tea(unsigned char toggles)
{
  tea_toggles = toggles;
  smoothnessServoStep = (SMOOTHNESS_TIME_SEC/(TEA_PACK_LIFTED_SERVO_VAL-TEA_PACK_IMMERSED_SERVO_VAL))*1000;
}

void tea_handler()
{
  static _t_dynamicTimer teaTimer;
  static unsigned char smoothnessServoVal = TEA_PACK_LIFTED_SERVO_VAL;
  static unsigned char servo_position_fl = 0; //0 - ������ ������� ������ , 1 - � ����� � �����
  
  if (! tea_toggles) return;
  
  if (! isTimerExpired( &teaTimer)) return;
  
  //�������� ������ �������
  if (! servo_position_fl)
  {
    //������ ��������
    if (smoothnessServoVal > TEA_PACK_IMMERSED_SERVO_VAL)
    {
      InitTimer( &teaTimer, smoothnessServoStep);
      servo_set(--smoothnessServoVal);
    }
    else //������� ���������� ��������
    { 
      InitTimer( &teaTimer, TEA_IMMERSED_DELAY);
      servo_position_fl = 1;
    }
  }
  else //��������� ������ �������
  { 
    //������ ���������
    if (smoothnessServoVal < TEA_PACK_LIFTED_SERVO_VAL)
    {
      InitTimer( &teaTimer, smoothnessServoStep);
      servo_set(++smoothnessServoVal);
    }
    else
    {
      InitTimer( &teaTimer, TEA_LIFTED_DELAY);
      servo_position_fl = 0;
      tea_toggles--;
    }
  }
}
