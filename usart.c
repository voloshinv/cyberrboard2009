#include "usart.h"
#include "iom16.h"
#include "global.h"
#include  <inavr.h>

#ifndef RXB8
#define RXB8 1
#endif

#ifndef TXB8
#define TXB8 0
#endif

#ifndef UPE
#define UPE 2
#endif

#ifndef DOR
#define DOR 3
#endif

#ifndef FE
#define FE 4
#endif

#ifndef UDRE
#define UDRE 5
#endif

#ifndef RXC
#define RXC 7
#endif

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)

// USART Receiver buffer
char rx_buffer[RX_BUFFER_SIZE];

// USART Transmitter buffer
char tx_buffer0[TX_BUFFER_SIZE0];

#if RX_BUFFER_SIZE<256
unsigned char rx_wr_index,rx_rd_index,rx_counter;
#else
unsigned int rx_wr_index,rx_rd_index,rx_counter;
#endif

#if TX_BUFFER_SIZE0<256
unsigned char tx_wr_index0,tx_rd_index0,tx_counter0;
#else
unsigned int tx_wr_index0,tx_rd_index0,tx_counter0;
#endif

// This flag is set on USART Receiver buffer overflow
U8 rx_buffer_overflow;

// USART Receiver interrupt service routine
#pragma vector=USART_RXC_vect
__interrupt void usart_rx_isr(void)
{
  char status,data;
  status=UCSRA;
  data=UDR;
  if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
  {
     rx_buffer[rx_wr_index]=data;
     if (++rx_wr_index == RX_BUFFER_SIZE) rx_wr_index=0;
     if (++rx_counter == RX_BUFFER_SIZE)
      {
        rx_counter=0;
        rx_buffer_overflow=1;
      };
   };
}

char usart_getchar(char *data)
{
  if (rx_counter==0) return 1;
  *data = rx_buffer[rx_rd_index];
  if (++rx_rd_index == RX_BUFFER_SIZE) rx_rd_index=0;
  __disable_interrupt();  //#asm("cli")
  --rx_counter;
  __enable_interrupt();   //#asm("sei")
  if (rx_buffer_overflow) return 2;
  return 0;
}

// USART Transmitter interrupt service routine
#pragma vector=USART_TXC_vect
__interrupt void usart0_tx_isr(void)
{
if (tx_counter0)
   {
   --tx_counter0;
   UDR=tx_buffer0[tx_rd_index0];
   if (++tx_rd_index0 == TX_BUFFER_SIZE0) tx_rd_index0=0;
   };
}

void usart_putchar(char c)
{
while (tx_counter0 == TX_BUFFER_SIZE0);
__disable_interrupt();  // #asm("cli")
if (tx_counter0 || ((UCSRA & DATA_REGISTER_EMPTY)==0))
   {
     tx_buffer0[tx_wr_index0]=c;
     if (++tx_wr_index0 == TX_BUFFER_SIZE0) tx_wr_index0=0;
     ++tx_counter0;
   }
else
   UDR=c;
__enable_interrupt();  //#asm("sei")
}

void usart_putstring(const char *str)
{
  while (*str)
  {
    usart_putchar(*str);
    str++;
  }
}

void usart_init()
{
  // USART initialization
  // Communication Parameters: 8 Data, 2 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud Rate: 115200
  UCSRA=0x00;
  UCSRB=0xD8;
  UCSRC=0x8E;
  UBRRH=0x00;
  UBRRL=0x08;
}
