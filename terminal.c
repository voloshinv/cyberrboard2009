#include "terminal.h"
#include "stdio.h"
#include <string.h>
#include "usart.h"
#include "lcd.h"
#include "servo.h"

extern char string_buff[];
extern int temperature;
extern U8 day, month, year, dow, hour, min, sec;

#define CMD_MAX_LEN	50
static char cmd[CMD_MAX_LEN];
static char cmd_index = 0;

#define TERM_COMMAND_QTY  8
const char *term_command[TERM_COMMAND_QTY] = {"help", "ping", "clearscreen", "temp", "date", "ledoff", "brightness", "maketea"};

void terminal_print_err(char *errormsg)
{
  usart_putstring("error: ");
  usart_putstring(errormsg);
}

void terminal_handler()
{
	char ch0;
	if (usart_getchar(&ch0) == 1) return;
	usart_putchar(ch0);
        
	cmd[cmd_index] = ch0;
	
	if (ch0 == '\r')
	{
                usart_putchar('\n');
                cmd[cmd_index] = 0;
		terminal_process_cmd();
                cmd_index = 0;
	}
	else
	{
		cmd_index++;
		if (cmd_index == CMD_MAX_LEN)
		{			
			terminal_print_err("command length is too long");
			cmd_index = 0;
		}
	}
	
	if (cmd_index == 0)
	{
		usart_putstring("\r\n>");
	}
}

void terminal_process_cmd()
{
	for (unsigned char icmd = 0; icmd < sizeof(term_command); icmd++)
	{
		if (! strcmp(cmd, term_command[icmd]))
		{
			terminal_do_command(icmd);
			return;
		}
	}
	terminal_print_err("command are not recognized!\r\nprint help for viewing available commands.");
}

void terminal_do_command(unsigned int cmd)
{
	if (cmd == 0)
	{
		usart_putstring("list of commands: ");
		for (unsigned char i=0; i < TERM_COMMAND_QTY; i++)
		{			
			usart_putstring(term_command[i]);
                        usart_putstring(" ");
		}
	}
	if (cmd == 1)
	{
		usart_putstring(" pong");
	}
	if (cmd == 2)
	{
		lcd_fillscreen(0x00F3);
	}
	if (cmd == 3)
	{
	        sprintf(string_buff, "T = %0.3f", temperature/16.0);
                usart_putstring(string_buff);
	}
	if (cmd == 4)
	{
                sprintf(string_buff, "%d/%d/20%d %d:%d:%d", day, month, year, hour, min, sec);
                usart_putstring(string_buff);
	}
	if (cmd == 5)
	{
		set_brightness(0);
	}
	if (cmd == 6)
	{
		//fl_set_brightness = 1;
	}
        if (cmd == 7)
	{
		make_tea(50);
                usart_putstring("making tea started!");
	}
}
