#ifndef AT24C512_LIBRARY
#define AT24C512_LIBRARY

#include "global.h"

#define AT24C512_ADDRESS  0xA0
#define AT24C512_PAGE_SIZE  128

U8 at24c_init();
U8 at24c_write_page(unsigned int page, unsigned char* buff);
U8 at24c_read_page(unsigned int page, unsigned char* buff);
U8 at24c_read8bytes(unsigned int address, unsigned char *buff);
U8 at24c_read(unsigned int address, unsigned char *data);

#endif
