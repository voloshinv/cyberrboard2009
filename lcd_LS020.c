#include "iom16.h"
#include "lcd.h"
#include  <inavr.h>
//#include "CharMap.h"
#include "At24C512_lib.h"
#include "stdio.h"

void spi_send16(unsigned int data)
{
  SPDR = data>>8;
  while(!(SPSR & (1<<SPIF)));

  SPDR = data;
  while(!(SPSR & (1<<SPIF)));
}

void lcd_wrcmd(unsigned int cmd)
{
  PORTD |= (1<<LCD_RS);
  spi_send16(cmd);
}


void lcd_wrdata(unsigned int data)
{
  PORTD &= ~(1<<LCD_RS);
  spi_send16(data);
}

void lcd_cshigh()
{
  PORTD |= (1<<LCD_CS);
  __no_operation();
  PORTD &= ~(1<<LCD_CS);
}

void lcd_fillscreen(unsigned int color)
{
unsigned int i;

  open_window(0, 0, 132, 176);
  for (i=0; i<DISP_W*DISP_H; i++)
    lcd_wrdata(color);
  
  lcd_cshigh();
}

void open_window(char x, char y, char x_len, char y_len){
/*
 0,0----> x=131
     |
     |
     V
   y=175
*/
  lcd_wrcmd(0xEF90);
  lcd_wrcmd(0x0504);
  lcd_wrcmd(0x0800+x);
  lcd_wrcmd(0x0A00+y);
  lcd_wrcmd(0x0900+x+x_len-1);
  lcd_wrcmd(0x0B00+y+y_len-1);
}

void put_string_6x8(char x, char y, char* chArr, int ColPix, int ColBgr){//Text 6 x 8
  int i = 0;
  int color;
  char b, mask, s;
  unsigned char font_8[8];
  
  open_window(x, y, 132 - x, 8);
  while (*(chArr + i)) {
    
    b = *(chArr + i);
    if (b < 0x20) s = 0x00;
    if (b >= 0x20) s = b - 0x20;
    if ((b >= 0x80) & (b < 0xC0)) s = 0x00;
    if (b >= 0xC0) s = b - 0x60;

    at24c_read8bytes(s*5, font_8);
    
    for (x = 0; x < 6; x++) {
      //b = font_6x8[s][x];
      //b = font_6x8[s*5 + x];
      b = font_8[x];
      
      if (x == 5) b = 0;
      mask = 0x01;
      for (y = 0; y < 8; y++) {
        if (b & mask) color = ColPix;
        else color = ColBgr;
        lcd_wrdata(color);
        mask <<= 1;
      }
    }
    i++;
  }
  lcd_cshigh();	// deselect display, high  
}

void put_string_8x8(char x, char y, char* chArr, int ColPix, int ColBgr){//Text 8 x 8
  int i = 0;
  int color;
  char b, mask, s;
  
  open_window(x, y, 132 - x, 8);
  while (*(chArr + i)) {
    mask = 0x80;
    
    s = *(chArr + i);
    
    unsigned char font_8[8];
    at24c_read8bytes((s << 3) + 896, font_8);

    for (x = 0; x < 8; x++) {
      
      for (y = 0; y < 8; y++) {
        //b = font_8x8[(s << 3) + y ];
        b = font_8[y];
        
        if (b & mask) color = ColPix;
        else color = ColBgr;
        lcd_wrdata(color);
        
      }
      mask >>= 1;
    }
    i++;
  }
  lcd_cshigh();	// deselect display, high  
}

void PWM_backlight_init()
{
  // Timer/Counter 2 initialization
  // Clock source: System Clock
  // Clock value: 15,625 kHz
  // Mode: Fast PWM top=FFh
  // OC2 output: Non-Inverted PWM
  ASSR=0x00;
  TCCR2=0x6F;
  TCNT2=0x00;
  OCR2=0x7F;
}

void lcd_ls020_init()
{
  //GPIO configuration
  DDRD |= (1<<LCD_RESET) | (1<<LCD_CS) | (1<<LCD_RS) | (1<<PD7);
  DDRB |= (1<<PB5) | (1<<PB7) | (1<<PB4); 

  //������� SPI ����
  PORTB |= (1<<PB4); //must be configured as an output and be high.
  PORTB |= (1<<LCD_MISO);  //enable pull up of MISO to avoid floating input
  SPCR |= (1<<SPE)|(1<<MSTR);  //Enable SPI, Master, set clock rate fck/4  
  SPSR |= (1<<SPI2X); //Set SPI clock rate fck/2
  
  //3.3V power enable
  DDRC |= 1<<PC6;
  PORTC &= ~(1<<PC6);

  PORTD &= ~(1<<LCD_RESET); //display reset  
  DELAY_MS(10);  //wait one ms to have a nice reset
  PORTD |= (1<<LCD_RESET); //release display reset

  //PORTD |= (1<<PD7); //�������� ��������� LCD
  PORTD &= ~(1<<LCD_CS);  //select Display
  PORTD |= (1<<LCD_RS);  //start with RS high (command)

  // Init 1
       spi_send16(0xFDFD);
       DELAY_US(10);
       spi_send16(0xFDFD);
       DELAY_MS(50);
// Init 2
       spi_send16(0xEF00);
       DELAY_US(10);
       spi_send16(0xEE04);
       DELAY_US(10);
       spi_send16(0x1B04);
       DELAY_US(10);
       spi_send16(0xFEFE);
       DELAY_US(10);
       spi_send16(0xFEFE);
       DELAY_US(10);
       spi_send16(0xEF90);
       DELAY_US(10);
       spi_send16(0x4A04);
       DELAY_US(10);
       spi_send16(0x7F3F);
       DELAY_US(10);
       spi_send16(0xEE04);
       DELAY_US(10);
       spi_send16(0x4306);
       DELAY_MS(7);
// Init 3
       spi_send16(0xEF90);
       DELAY_US(10);
       spi_send16(0x0983);
       DELAY_US(10);
       spi_send16(0x0800);
       DELAY_US(10);
       spi_send16(0x0BAF);
       DELAY_US(10);
       spi_send16(0x0A00);
       DELAY_US(10);
       spi_send16(0x0500);
       DELAY_US(10);
       spi_send16(0x0600);
       DELAY_US(10);
       spi_send16(0x0700);
       DELAY_US(10);
       spi_send16(0xEF00);
       DELAY_US(10);
       spi_send16(0xEE0C);
       DELAY_US(10);
       spi_send16(0xEF90);
       DELAY_US(10);
       spi_send16(0x0080);
       DELAY_US(10);
       spi_send16(0xEFB0);
       DELAY_US(10);
       spi_send16(0x4902);
       DELAY_US(10);
       spi_send16(0xEF00);
       DELAY_US(10);
       spi_send16(0x7F01);
       DELAY_US(10);
       spi_send16(0xE181);
       DELAY_US(10);
       spi_send16(0xE202);
       DELAY_US(10);
       spi_send16(0xE276);
       DELAY_US(10);
       spi_send16(0xE183);
       DELAY_US(10);
       spi_send16(0x8001);
       DELAY_US(10);
       spi_send16(0xEF90);
       DELAY_US(10);
       spi_send16(0x0000);
       
  lcd_cshigh();
  
  PWM_backlight_init();
}

void set_brightness(unsigned char percent)
{
  if (percent > 100) return;
  
  OCR2 = (int)(percent * 2.5);
}

#define BUTTONS_BLOCK_DELAY 150
static unsigned char br = 50;

void set_brightness_up()
{
  if (br <= 90)
  { 
    br +=10;
    show_brightness_to_user(br);
    DELAY_MS(BUTTONS_BLOCK_DELAY);
  }
}

void set_brightness_down()
{
  if (br >= 10)
  { 
    br -=10;
    show_brightness_to_user(br);
    DELAY_MS(BUTTONS_BLOCK_DELAY);
  }
}

extern char string_buff[50];
void show_brightness_to_user(char percent)
{
  sprintf(string_buff, "������� = %d%% ", percent);
  set_brightness(percent);
  put_string_8x8(10, 150, string_buff, RED, 0x00F3);
}
