#ifndef SERVO_MODULE_
#define SERVO_MODULE_

void servo_init();
void servo_set(unsigned char percent);
void servo_go();

void tea_handler();
void make_tea(unsigned char toggles);

#endif // SERVO_MODULE_