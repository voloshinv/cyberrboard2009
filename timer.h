#ifndef TIMER_GLOBAL_
#define TIMER_GLOBAL_

#include "global.h"

typedef struct
{
  unsigned long overflows_val;
  unsigned int timer_val;
} _t_dynamicTimer;

void global_timer_init();
void InitTimer( _t_dynamicTimer *tim, unsigned long time_ms);
U8 isTimerExpired( _t_dynamicTimer *tim);

#endif