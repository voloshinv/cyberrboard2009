#include  <inavr.h>
#include "iom16.h"
#include "At24C512_lib.h"
#include "twi.h"
#include "usart.h"
#include "stdio.h"

/*
  ��������! 
  7 ������ ������� (800 ����) ������ ��� ����� 6�8 !!!
  7-23 �������� ������ ��� ����� 8x8 ( 2048 ����) !!!
*/

extern char string_buff[];

U8 recieve_at24c_page_buffer_flag = 1;
U8 at24c_page_buffer[AT24C512_PAGE_SIZE], at24c_page_buffer_index = 0;
U8 page_writing_index = 7; //�������� ������ � 7�� ��������

U8 at24c_check_flag = 0;

U8 at24c_init()
{
  U8 result;
  result =  i2c_start(AT24C512_ADDRESS | TWI_WRITE);
  if (result) return 1;
  
  at24c_check_flag = 1;
  return 0;
}

U8 at24c_write_page(unsigned int page, unsigned char* buff)
{
  if (! at24c_check_flag) return 1;
  
  if (page < 23) return 2;
  
  i2c_start(AT24C512_ADDRESS | TWI_WRITE);
  //write address
  page <<= 7; //������������ * 128
  i2c_write(page>>8); //address 1
  i2c_write(page); //address 2
  //write data
  for (int i=0; i<128; i++)
  {
    i2c_write(buff[i]);
    DELAY_MS(3);
  }

  i2c_stop();

  return 0;  
}

U8 at24c_read_page(unsigned int page, unsigned char* buff)
{
  if (! at24c_check_flag) return 1;
  
  i2c_start(AT24C512_ADDRESS | TWI_WRITE);
  //write address
  page <<= 7; //������������ * 128
  i2c_write(page>>8); //address 1
  i2c_write(page); //address 2
  //read data
  i2c_rep_start(AT24C512_ADDRESS | TWI_READ);
  for (int i=0; i<128; i++)
  {
    buff[i] = i2c_readAck();
  }
  
  i2c_stop();
  
  return 0;
}

U8 at24c_read8bytes(unsigned int address, unsigned char *buff)
{
  if (! at24c_check_flag) return 1;
  
  i2c_start(AT24C512_ADDRESS | TWI_WRITE);
  
  //write address
  i2c_write(address>>8); //address 1
  i2c_write(address); //address 2
  
  //read data
  i2c_rep_start(AT24C512_ADDRESS | TWI_READ);
  
  int i;
  for (i=0; i<7; i++)
    buff[i] = i2c_readAck();

  buff[i] = i2c_readNak();
  
  i2c_stop();
  
  return 0;
}

U8 at24c_read(unsigned int address, unsigned char *data)
{
  if (! at24c_check_flag) return 1;
  
  i2c_start(AT24C512_ADDRESS | TWI_WRITE);
  //write address
  i2c_write(address>>8); //address 1
  i2c_write(address); //address 2
  //read data
  i2c_rep_start(AT24C512_ADDRESS | TWI_READ);
  *data = i2c_readNak();
  
  i2c_stop();
  
  return 0;
}

void at24c_wr_handler()
{
  U8 data;
    if (! usart_getchar((char*)&data))
    {
      at24c_page_buffer[at24c_page_buffer_index] = data;
      if (at24c_page_buffer_index+1 == AT24C512_PAGE_SIZE)
      {
        at24c_page_buffer_index = 0;
        //do writing
        at24c_write_page( page_writing_index, at24c_page_buffer);
        
        sprintf(string_buff, "written %d bytes", page_writing_index);
        usart_putstring(string_buff);
        
        page_writing_index++;
      }
      else
        at24c_page_buffer_index++;
    }
}
