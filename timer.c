#include "timer.h"
#include  <inavr.h>
#include "stdio.h"
#include "global.h"
#include "iom16.h"

static unsigned long globalTimerOverflowsVal=0;
// Timer1 overflow interrupt service routine
#pragma vector=TIMER1_OVF_vect
__interrupt void timer1_ovf_isr(void)
{
  globalTimerOverflowsVal++;
}

// �������� 1clk ������� ����� 0,000064� (64 ���).
// ���� owerflow ������� ����� 65535 * 1 clk = 4,19424 c = 4194240 ���
void InitTimer( _t_dynamicTimer *tim, unsigned long time_ms)
{
  tim->overflows_val = time_ms/4194;
  
  time_ms *= 16; //15.625 - ��������� ���������� ����� ��� ������������
  //�� ������ ����� time_ms ��������� ��� ���������� ����� (ms * ticks);  
  tim->timer_val = time_ms - (tim->overflows_val*4194);
 
  U8 inter = __save_interrupt();
  __disable_interrupt();
  unsigned int current_timer_val = TCNT1;
  tim->overflows_val += globalTimerOverflowsVal;
  __restore_interrupt(inter);
  
  unsigned long ovf_check = tim->timer_val;
  ovf_check += current_timer_val;
  if (ovf_check >= 0xFFFF) tim->overflows_val++;
  tim->timer_val += current_timer_val;
}

U8 isTimerExpired( _t_dynamicTimer *tim)
{
  //static unsigned int inter;
  static unsigned long current_overflows;
  static unsigned int current_timer_val;
  
  //inter = __save_interrupt();
  //����� ������� ��������� �������, � ����� ������
  TCCR1B=0x00;  //__disable_interrupt();
  current_overflows = globalTimerOverflowsVal;
  current_timer_val = TCNT1;
  TCCR1B=0x05;  //__restore_interrupt(inter);
  if (tim->overflows_val < current_overflows) return 1;
  if ((tim->overflows_val == current_overflows) && (tim->timer_val <= current_timer_val)) return 1;
  return 0;
}

void global_timer_init()
{
  // Timer/Counter 1 initialization
  // Clock source: System Clock
  // Clock value: 15,625 kHz
  // Mode: Normal top=FFFFh
  // OC1A output: Discon.
  // OC1B output: Discon.
  // Noise Canceler: Off
  // Input Capture on Falling Edge
  // Timer1 Overflow Interrupt: On
  // Input Capture Interrupt: Off
  // Compare A Match Interrupt: Off
  // Compare B Match Interrupt: Off
  TCCR1A=0x00;
  TCCR1B=0x05;
  TCNT1H=0x00;
  TCNT1L=0x00;
  ICR1H=0x00;
  ICR1L=0x00;
  OCR1AH=0x00;
  OCR1AL=0x00;
  OCR1BH=0x00;
  OCR1BL=0x00;
  
  // Timer(s)/Counter(s) Interrupt(s) initialization
  TIMSK=0x04;  
}
