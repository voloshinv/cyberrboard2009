#ifndef TWI_MODULE
#define TWI_MODULE

/* I2C clock in Hz */
#define TWI_SCL_CLOCK  100000L

/** defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start() */
#define TWI_READ    1

/** defines the data direction (writing to I2C device) in i2c_start(),i2c_rep_start() */
#define TWI_WRITE   0

/* TWSR values (not bits) */
/* Master */
#define TW_START		0x08
#define TW_REP_START		0x10
/* Master Transmitter */
#define TW_MT_SLA_ACK		0x18
#define TW_MT_SLA_NACK		0x20
#define TW_MT_DATA_ACK		0x28
#define TW_MT_DATA_NACK		0x30
#define TW_MT_ARB_LOST		0x38
/* Master Receiver */
#define TW_MR_ARB_LOST		0x38
#define TW_MR_SLA_ACK		0x40
#define TW_MR_SLA_NACK		0x48
#define TW_MR_DATA_ACK		0x50
#define TW_MR_DATA_NACK		0x58

//максимальное количество итераций внутри бесконечных циклов опроса регистров i2c
#define TWI_POOL_LIMIT 100000

void i2c_init(void);
unsigned char i2c_start(unsigned char address);
void i2c_start_wait(unsigned char address);
unsigned char i2c_rep_start(unsigned char address);
void i2c_stop(void);
unsigned char i2c_write( unsigned char data );
unsigned char i2c_readAck(void);
unsigned char i2c_readNak(void);

#endif
